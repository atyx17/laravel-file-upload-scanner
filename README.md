### Local dev
 - create .env
 - complete db credentials
 - composer install
 - php artisan key:generate
 - php artisan migrate
 - php artisan serve
 - open: http://127.0.0.1:8000/upload-file
 - upload the test file: virustest.txt
